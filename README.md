The given R script generates the word cloud based on 1000 tweets (having #HappyNewYear2018) gathered from twitter like given below.

![Alt text](TwitterCloud.png?raw=true "Twitter Cloud")

You can change the text to be searched (like # tag) and number of tweets to be fetched.
Happy New Year to all of You.
