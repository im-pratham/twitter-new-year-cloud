# add your keys 
app_key = 
app_secret = 
access_token = 
access_secret = 

# load library. (if you did't have following libraries, install them using install.packages())
library(twitteR)
library(ROAuth)

credential<-OAuthFactory$new(consumerKey=app_key,
                             consumerSecret=app_secret,
                             requestURL="https://api.twitter.com/oauth/request_token",
                             accessURL="https://api.twitter.com/oauth/access_token",
                             authURL="https://api.twitter.com/oauth/authorize")

credential$handshake()

# Twitter authentication
setup_twitter_oauth(app_key, app_secret, access_token, access_secret)

# search for topic of interest
search.string = '#HappyNewYear2018'
no.of.tweets = 1000

# retrieve tweets
tweets = searchTwitter(search.string, n=no.of.tweets, lang="en")
print(tweets)

# convert tweets to a data frame
tweets.df = twListToDF(tweets)

# cleaning text i.e. removing stopwords, blanks etc
library(tm)
tweets.corpus = VCorpus(VectorSource(tweets.df$text))

remove_emoticon = function(x) {
  gsub('[^\x01-\x7F]', '', x)
}
tweets.corpus = tm_map(tweets.corpus, content_transformer(remove_emoticon))
tweets.corpus = tm_map(tweets.corpus, content_transformer(tolower))

# create function to remove url
removeURL = function(x) {
  gsub('http[^[:space:]]*', '', x)
}
tweets.corpus = tm_map(tweets.corpus, content_transformer(removeURL))

# remove punctuations and numbers. i.e. keep only alphabets
removeNumPunct = function(x) {
  gsub('[^[:alpha:][:space:]]*', '', x)
}
tweets.corpus = tm_map(tweets.corpus, content_transformer(removeNumPunct))

tweets.corpus = tm_map(tweets.corpus, removeWords, stopwords())
# remove rt word
tweets.corpus = tm_map(tweets.corpus, removeWords, c('rt'))
tweets.corpus = tm_map(tweets.corpus, stripWhitespace)

# copy tweet.corpus
tweets.corpus.copy = tweets.corpus

# stem corpus
library(SnowballC)
tweets.corpus = tm_map(tweets.corpus, stemDocument)
stemCompletion2 <- function(x, dictionary) {
  x <- unlist(strsplit(as.character(x), " "))
  x <- x[x != ""]
  x <- stemCompletion(x, dictionary=dictionary)
  x <- paste(x, sep="", collapse=" ")
  PlainTextDocument(stripWhitespace(x))
}
tweets.corpus = lapply(tweets.corpus, stemCompletion2, dictionary = tweets.corpus.copy)
tweets.corpus = VCorpus(VectorSource(tweets.corpus))
tweets.tdm = TermDocumentMatrix(tweets.corpus, control = list(wordLengths = c(1, Inf)))

# make wordcloud from data
library(wordcloud)
wordcloud(tweets.corpus, min.freq = 3,
          max.words=200, random.order=FALSE, rot.per=0.35, 
          colors=brewer.pal(8, "Dark2"))
